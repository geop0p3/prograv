#include "bitvector.h"

bitvector::bitvector(int _tamanioInicial) {
	arreglo_ = 0;
	tamanio_ = 0;

	CambiarTamanio(_tamanioInicial); //creamos arreglo
	SetAllFalse(); //iniciamos todo el arreglo a false
}

bitvector::~bitvector() {
	if (tamanio_ > 0) //tenemos datos?
		delete[] arreglo_; //liberamos memoria
}

int bitvector::Tamanio() {
	return tamanio_ * 32; //tamanio tiene mi cantidad ints, y cada int puede almacenar 32 booleanos
}

void bitvector::CambiarTamanio(int _nuevoTamanio) {
	unsigned long int* nuevoArreglo = 0; //creamos nuevo sector de memoria RAM para arreglo

	int nuevoTamanio;
	if (_nuevoTamanio % 32 == 0) { //si es multiplo de 32 ta cual
		nuevoTamanio = _nuevoTamanio / 32;
	}
	else {
		nuevoTamanio = (_nuevoTamanio / 32) + 1; // en caso contrario ocupamos sumar + 1
	}

	nuevoArreglo = new unsigned long int[nuevoTamanio];

	int tamanioMin = tamanio_ < nuevoTamanio ? tamanio_ : nuevoTamanio; //obtenemos que arreglo es mas pequenio

	//respaldamos datos del viejo arreglo al nuevo
	for (int i = 0; i < tamanioMin; i++) {
		nuevoArreglo[i] = arreglo_[i];
	}

	//actualizamos nuestras variables
	arreglo_ = nuevoArreglo;
	tamanio_ = nuevoTamanio;
}

bool bitvector::operator[](int _index) { //nos permite trabajar la clase como a[index]
	int celda = _index / 32;
	int bit = _index % 32;
	return (arreglo_[celda] & (1 << bit)) >> bit; //nos devuelve solo el bit que deseamos
}

void bitvector::Establecer(int _index, bool _valor) {
	int celda = _index / 32;
	int bit = _index % 32;

	//si _valor es true usamos or y si es false usamos and
	if (_valor) {
		arreglo_[celda] = (arreglo_[celda] | (1 << bit)); //ese bit lo hacemos true
	}
	else {
		arreglo_[celda] = (arreglo_[celda] & (~(1 << bit))); //ese bit lo hacemos falso
	}
}

void bitvector::SetAllFalse() {
	for (int i = 0; i < tamanio_; i++) {
		arreglo_[i] = 0;
	}
}