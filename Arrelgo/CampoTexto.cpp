#include "CampoTexto.h"

CampoTexto::CampoTexto(const std::string& _text, Resources* _resource, spInputText _input)
{
	text = new TextField;
	//Don't handle touch events on this Actor
	text->setTouchEnabled(false);

	TextStyle style;
	style.color = Color::Black;
	style.hAlign = TextStyle::HALIGN_MIDDLE;
	style.vAlign = TextStyle::VALIGN_MIDDLE;
	style.multiline = true;
	style.font = _resource->getResFont("main");
	text->setStyle(style);
	text->setText(_text);
	addChild(text);

	setSize(200, 60);
	addEventListener(TouchEvent::CLICK, CLOSURE(this, &CampoTexto::onClick));

	input_ = _input; //Respaldamos controlador de teclado
}

void CampoTexto::sizeChanged(const Vector2& size)
{
	text->setSize(size);
}

int CampoTexto::GetTextoInt()
{
	int numero = std::stoi(text->getText());
	return numero;
}

void CampoTexto::onClick(Event* ev)
{
	input_->start(text);
}