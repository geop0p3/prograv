#include "BitBox.h"

BitBox::BitBox(oxygine::Resources &_gameResources, oxygine::Vector2 _pos) {
	setResAnim(_gameResources.getResAnim("square"));
	setPosition(_pos);

	this->addEventListener(oxygine::TouchEvent::TOUCH_DOWN, CLOSURE(this, &BitBox::touched));

	SetColorRed();
}

void BitBox::doUpdate(const oxygine::UpdateState &us)
{

}

void BitBox::touched(oxygine::Event* event) {

	//bitvec.Establecer(1, true);
	m = vec->operator[](index);

	if (m) {
		SetColorRed();
		vec->Establecer(index, false);
	}
	else {
		SetColorGreen();
		vec->Establecer(index, true);
	}
}

void BitBox::SetColorGreen() {

	setColor(oxygine::Color::Green);

}

void BitBox::SetColorRed() {
	setColor(oxygine::Color::Red);
}