#pragma once

class bitvector {
public:
	bitvector(int _tamanioInicial);
	~bitvector();

	int Tamanio(); //regresa tamanio del bitvector

	void CambiarTamanio(int _nuevoTamanio);
	bool operator[](int _index); //nos permite trabajar la case como a[index]
	void Establecer(int _index, bool _valor);
	void SetAllFalse(); //Setea todo mi arreglo a false

	unsigned long int* arreglo_;
	int tamanio_;

private:
	int i = 0;
};