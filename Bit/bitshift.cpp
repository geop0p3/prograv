#include "oxygine-framework.h"
#include <functional>
#include "bitvector.h"
#include "BitBox.h"
#include <iostream>

oxygine::Resources gameResources;
bitvector bitvec(32);
spBit bit;

int i = 0;
int j = 0;
int count = 0;

void bitshift_preinit() {}

//called from main.cpp
void bitshift_init()
{
	//load xml file with resources definition
	gameResources.loadXML("res.xml");

	bitvec.SetAllFalse();

	//row 1
	for (i = 0; i < 8; i++) {
		bit = new BitBox(gameResources, oxygine::Vector2(100 + (i * 30), 100));
		oxygine::getStage()->addChild(bit);
		bit->vec = &bitvec;
		bit->index = i;
	}

	//row 2
	for (i = 8; i < 16; i++) {
		bit = new BitBox(gameResources, oxygine::Vector2(100 + (count * 30), 200));
		oxygine::getStage()->addChild(bit);
		bit->vec = &bitvec;
		bit->index = i;
		count++;
	}

	count = 0;
	//row 3
	for (i = 16; i < 24; i++) {
		bit = new BitBox(gameResources, oxygine::Vector2(100 + (count * 30), 300));
		oxygine::getStage()->addChild(bit);
		bit->vec = &bitvec;
		bit->index = i;
		count++;
	}
	
	count = 0;
	//row 4
	for (i = 24; i < 32; i++) {
		bit = new BitBox(gameResources, oxygine::Vector2(100 + (count * 30), 400));
		oxygine::getStage()->addChild(bit);
		bit->vec = &bitvec;
		bit->index = i;

		count++;
	}


}

//called each frame from main.cpp
void bitshift_update()
{
	const Uint8* data = SDL_GetKeyboardState(0);

	
}

//called each frame from main.cpp
void bitshift_destroy()
{
	//free previously loaded resources
	gameResources.free();
}