#ifndef LISTALIGADA
#define LISTALIGADA

template <class T>
class Nodo{
  public:
    T valor;
    Nodo<T>* siguiente;
};

template<class T>
class ListaLigada{
  public:
    ListaLigada(){
      cabeza=NULL;
      cola = NULL;
      cantidad = 0;
    }

    ~ListaLigada(){
      Nodo<T>* itr = cabeza;
      Nodo<T>* next;
      while(itr != NULL){ //Borramos toda nuestra lista ligada
        next=itr->siguiente; //Respaldo el siguiente.
        delete itr;  //Borramos el valor
        itr=next;  //Avanzamos al siguiente elemento
      }
    }
    
    void AgregarAlFinal(T _valor){
      if(cabeza == NULL){
        cabeza = new Nodo<T>;
        cabeza->valor = _valor;
      } else {
        Nodo<T>* tmp = new Nodo<T>;
        tmp->valor = _valor;
        cola->siguiente = tmp;
        cola = tmp;
      }
      
      cantidad++;
    }
    
    void AgregarAlInicio(T _valor){
      Nodo<T>* tmp = new Nodo<T>;
      tmp->valor = _valor;
      tmp->siguiente = cabeza;
      cabeza = tmp;
      if(cola == NULL){
        cola = cabeza;
      }
      
      cantidad++;
    }
    
    void AgregarAt(int _pos, T _valor){
      if(_pos >= cantidad){
        return;
      }
      
      Nodo<T>* itr = cabeza;
      for (int i = 0; i < _pos -1; i++){
        itr = itr -> siguiente;
      }
      
      Nodo<T>* nuevo = new Nodo<T>;
      nuevo -> valor = _valor;
      nuevo -> siguiente= itr -> siguiente;
      itr -> siguiente = nuevo;
      if(cola == itr){
        cola -> siguiente = nuevo;
      }
      
      cantidad++;
    }
    
    void BorrarCabeza(){
      Nodo<T>* nodo = NULL;
      if (cabeza != NULL){
        nodo = cabeza -> siguiente;
        delete cabeza;
        cabeza = nodo;
        cantidad--;
      }
      
    }
    
    void BorrarCola(){
      Nodo<T>* nodo = cabeza;
      if (cabeza != NULL) {
        if (cabeza == cola) {
          delete cabeza;
          cola = cabeza = NULL;
        } else {
          while (nodo -> siguiente != cola) {
            nodo = nodo -> siguiente;
          }
          
          delete cola;
          cola = nodo;
          cola -> siguiente = NULL;
        }
        
        cantidad --;
      }
    }
    
    void BorrarAt (int _pos) {
      if (cabeza == NULL){
        return;
      }
      
      if (_pos >= cantidad) {
        return;
      }
      
      Nodo<T>* nodo = cabeza;
      
      for (int i=0; i < _pos -1; i++) {
        nodo = nodo->siguiente;
      }
      
      Nodo<T>* tmp = nodo -> siguiente;
      nodo -> siguiente = tmp -> siguiente;
      delete tmp;
      
      if (nodo -> siguiente == NULL) {
        cola = nodo;
      }
      
      cantidad--;
    }
    
    int Tamano () {
      return cantidad;
    }
    
    Nodo<T>* GetCabeza () {
      return cabeza;
    }
    
    Nodo<T>* GetAt (int _pos) {
      if (cabeza == NULL){
        return NULL;
      }
      
      if (_pos >= cantidad) {
        return NULL;
      }
      
      Nodo<T>* itr = cabeza;
      for (int i = 0; i < _pos -1; i++) {
        itr = itr -> siguiente;
      }
      
      return itr;
    }
    
  private:
    Nodo<T>* cabeza;
    Nodo<T>* cola;
    int cantidad;
};

#endif
