#pragma once
#include "oxygine-framework.h"
using namespace oxygine;

/*
Como usar:
En example declarar:
spInputText input_; //Solo se necesita 1 sin importar cantidad de CamposTexto
spCampoTexto input_texto;

//Agregar la siguiente funci�n en example.cpp
void onComplete(Event* ev)
{
	InputText::stopAnyInput();
}

//Inicializar input_ en el inicalizar de example
input_ = new InputText;
input_->setAllowedSymbols("1234567890"); //Solo numeros
input_->addEventListener(Event::COMPLETE, CLOSURE(this, &MainActor::onComplete));

//Usar clase desde example
input_texto = new CampoTexto("Escribe un numero", &gameResources, input_);
input_texto->setPosition(300, 100);
input_texto->attachTo(this);

//para obtener texto
input_texto->text-getText();
*/


DECLARE_SMART(CampoTexto, spCampoTexto);
class CampoTexto : public ColorRectSprite
{
private:
	void onClick(Event* ev);

public:
	CampoTexto(const std::string& _text, Resources* _resource, spInputText _input);


	void sizeChanged(const Vector2& size);
	int GetTextoInt();


	spTextField text;
	spInputText input_;
};