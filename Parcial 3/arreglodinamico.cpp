#include <iostream>
#include <string>

using namespace std;

int tamano = 0;
int posicion = 0;
int eleccion = 0;
int valor = 0;
bool loop = true;
int nuevotamano = 0;

int main() {
  cout << "Ingresa el tamano inicial del arreglo ";
  cin >> tamano;
  int* arreglo = new int [tamano];
  for (int i = 0; i < tamano; ++i) {
    cout << arreglo[i] << '\n';
  }
  while (loop) {
    cout << "Ingresa 1 para cambiar un valor o 2 para cambiar el tamano del "
"arreglo. Ingresa 0 para salir ";
    cin >> eleccion;
    if (eleccion == 1) {
      cout << "Ingresa la posicion del valor ";
      cin >> posicion;
      cout << "Ingresa el nuevo valor ";
      cin >> valor;
      arreglo[posicion] = valor;
      for (int i = 0; i < tamano; ++i) {
        cout << arreglo[i] << '\n';
      }
    } else if (eleccion == 2) {
      cout << "Escribe el nuevo tamano del arreglo ";
      cin >> nuevotamano;
      int* arreglotemp = new int [nuevotamano];
      for (int i =0; i < tamano; i++) {
        arreglotemp[i] = arreglo[i];
      }
      delete [] arreglo;
      arreglo = arreglotemp;
      // Imprime
      for (int i = 0; i < nuevotamano; ++i) {
        cout << arreglo[i] << '\n';
      }
    } else if (eleccion == 0) {
      loop = false;
    }
  }
  return 0;
}
