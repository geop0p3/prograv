#include "oxygine-framework.h"
#include <functional>
#include <iostream>
#include "CampoTexto.h"

oxygine::Resources gameResources;

spInputText input_; //Solo se necesita 1 sin importar cantidad de CamposTexto
spCampoTexto input_texto;

void arreglos_preinit() {}

class MainActor : public oxygine::Actor
{
public:
	//spBullet bullet;
	int count = 0;
	MainActor()
	{
		input_ = new InputText;
		input_->setAllowedSymbols("1234567890"); //Solo numeros
		input_->addEventListener(Event::COMPLETE, CLOSURE(this, &MainActor::onComplete));

		input_texto = new CampoTexto("Escribe un numero", &gameResources, input_);
		input_texto->setPosition(300, 100);
		input_texto->attachTo(this);
	}

	void onComplete(Event* ev)
	{
		InputText::stopAnyInput();
	}
};

typedef oxygine::intrusive_ptr<MainActor> spMainActor;
//called from main.cpp
spMainActor actor;

void arreglos_init()
{
	//load xml file with resources definition
	gameResources.loadXML("res.xml");

	actor = new MainActor;

	//and add it to Stage as child
	oxygine::getStage()->addChild(actor);
}

//called each frame from main.cpp
void arreglos_update()
{
	const Uint8* data = SDL_GetKeyboardState(0);
}

//called each frame from main.cpp
void arreglos_destroy()
{
	//free previously loaded resources
	gameResources.free();
}