#include <iostream>
#include <string>
#include "ListaLigada.h"

using namespace std;


int main () {
   ListaLigada<int> lista;
   lista.AgregarAlInicio(1);
   lista.AgregarAlFinal(5);
   lista.AgregarAlFinal(7);
   lista.AgregarAt(1,3);
   
   lista.BorrarCabeza();
   lista.AgregarAlInicio(2);
   
   lista.BorrarCola();
   
   lista.BorrarAt(1);
   
   Nodo<int>* itr = lista.GetCabeza();
   while (itr != NULL) {
     cout << itr -> valor << endl;
     itr = itr -> siguiente;
   }
   
   cout << "Tamano Final: " << lista.Tamano() << endl;
   
   return 0;
}
