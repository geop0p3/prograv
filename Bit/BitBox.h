#pragma once

#include "oxygine-framework.h"
#include "bitvector.h"

class BitBox : public oxygine::Sprite
{
public:
	BitBox(oxygine::Resources &_gameResources, oxygine::Vector2 _pos);
	void SetColorGreen();
	void SetColorRed();

	bitvector *vec;

	int index;
	bool state;
	void touched(oxygine::Event*);

private:
	bool m;

protected:
	void doUpdate(const oxygine::UpdateState &us);
};

typedef oxygine::intrusive_ptr<BitBox> spBit;